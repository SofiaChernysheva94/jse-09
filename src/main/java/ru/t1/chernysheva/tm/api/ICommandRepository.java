package ru.t1.chernysheva.tm.api;

import ru.t1.chernysheva.tm.model.Command;

public interface ICommandRepository {

    Command[] getCommands();

}
