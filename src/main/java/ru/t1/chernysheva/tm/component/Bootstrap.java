package ru.t1.chernysheva.tm.component;

import ru.t1.chernysheva.tm.constant.CommandConst;
import ru.t1.chernysheva.tm.api.ICommandController;
import ru.t1.chernysheva.tm.api.ICommandRepository;
import ru.t1.chernysheva.tm.api.ICommandService;
import ru.t1.chernysheva.tm.constant.ArgumentsConst;
import ru.t1.chernysheva.tm.controller.CommandController;
import ru.t1.chernysheva.tm.repository.CommandRepository;
import ru.t1.chernysheva.tm.service.CommandService;
import ru.t1.chernysheva.tm.util.TerminalUtil;

public final class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    private void processCommands() {
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        while (!Thread.currentThread().isInterrupted()) {
            System.out.println("ENTER COMMAND: ");
            final String command = TerminalUtil.nextLine();
            processCommand(command);
        }
    }

    private void processArguments(final String[] arguments) {
        if (arguments == null || arguments.length < 1) return;
        processArgument(arguments[0]);
        exit();
    }

    private void exit () {
        System.exit(0);
    }

    private void processCommand(final String argument) {
        switch (argument) {
            case CommandConst.VERSION:
                commandController.showVersion();
                break;
            case CommandConst.ABOUT:
                commandController.showAbout();
                break;
            case CommandConst.INFO:
                commandController.showSystemInfo();
                break;
            case CommandConst.HELP:
                commandController.showHelp();
                break;
            case CommandConst.EXIT:
                exit();
                break;
            default:
                commandController.showErrorCommand();
        }
    }

    private void processArgument(final String argument) {
        switch (argument) {
            case ArgumentsConst.VERSION:
                commandController.showVersion();
                break;
            case ArgumentsConst.ABOUT:
                commandController.showAbout();
                break;
            case ArgumentsConst.INFO:
                commandController.showSystemInfo();
                break;
            case ArgumentsConst.HELP:
                commandController.showHelp();
                break;
            default:
                commandController.showErrorArgument();
        }
    }

    public void run(final String... args) {
        processArguments(args);
        processCommands();
    }

}