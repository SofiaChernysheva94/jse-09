package ru.t1.chernysheva.tm.util;

import ru.t1.chernysheva.tm.constant.MeasureConst;

public interface FormatUtil {

    static String formatBytes(long bytes) {
        if ((bytes >= 0) && (bytes < MeasureConst.KB)) {
            return bytes + " B";
        } else if ((bytes >= MeasureConst.KB) && (bytes < MeasureConst.MB)) {
            return (bytes / MeasureConst.KB) + " KB";
        } else if ((bytes >= MeasureConst.MB) && (bytes < MeasureConst.GB)) {
            return (bytes / MeasureConst.MB) + " MB";
        } else if ((bytes >= MeasureConst.GB) && (bytes < MeasureConst.TB)) {
            return (bytes / MeasureConst.GB) + " GB";
        } else if (bytes >= MeasureConst.TB) {
            return (bytes / MeasureConst.TB) + " TB";
        } else {
            return bytes + " Bytes";
        }
    }

}
