package ru.t1.chernysheva.tm.repository;

import ru.t1.chernysheva.tm.api.ICommandRepository;
import ru.t1.chernysheva.tm.constant.ArgumentsConst;
import ru.t1.chernysheva.tm.constant.CommandConst;
import ru.t1.chernysheva.tm.model.Command;

public final class CommandRepository implements ICommandRepository {

    private static final Command HELP = new Command(CommandConst.HELP, ArgumentsConst.HELP, "Show command list.");

    private static final Command VERSION = new Command(CommandConst.VERSION, ArgumentsConst.VERSION, "Show version info.");

    private static final Command ABOUT = new Command(CommandConst.ABOUT, ArgumentsConst.ABOUT, "Show developer info.");

    private static final Command INFO = new Command(CommandConst.INFO, ArgumentsConst.INFO, "Show system information.");

    private static final Command EXIT = new Command(CommandConst.EXIT, null, "Close application.");

    private static final Command[] COMMANDS = new Command[]{
            HELP, VERSION, ABOUT, INFO, EXIT
    };

    public Command[] getCommands() {
        return COMMANDS;
    }

}
